package exercise;


public class Exercise4 {

    public static int maxFraction(int [] numrators, int [] denominators){
        if (numrators.length != denominators.length){
            return -1;
        }
        int count = 1;
        int indexres = 0;
        while(count < numrators.length){
            if(numrators[indexres]*denominators[count] - numrators[count]*denominators[indexres] <= 0){
                indexres = count;
            }
            count++;
        }
        return indexres;
    }

    public static void main(String[] args) {
        int [] num = {5, 2, 5};
        int [] den = {6, 3, 4};
        int index = maxFraction(num, den);
        System.out.println(index);
    }
}
